package com.example.RxChatbot;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class MainViewModel implements IMainViewModel {

    final PublishSubject<Msg> msgPublishSubject = PublishSubject.create();

    @Override
    public Observable<Msg> getMsgSubject() {
        return msgPublishSubject;
    }

    @Override
    public void onMessageSend(String text) {
        final Msg myMsg = new Msg(text);
        msgPublishSubject.onNext(myMsg);

        NetworkService.getInstance()
                .getJSONApi()
                .getResponse(new BodyReq(text))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(DisposableSingleObserverUtils.getSingleObserver(
                        this::handleMsgReceive,
                        this::handleError
                ));
    }

    private void handleError(Throwable throwable) {
        final Msg errMsg = new Msg("ЧТО-ТО ПОШЛО НЕ ТАК");
        errMsg.setIsRequest(true);
        msgPublishSubject.onNext(errMsg);
    }

    private void handleMsgReceive(Msg msg) {
        msg.setIsRequest(true);
        msgPublishSubject.onNext(msg);
    }
}
