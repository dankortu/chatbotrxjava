package com.example.RxChatbot;


import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface NodeServerApi {
    @POST("/")
    Single<Msg> getResponse(@Body BodyReq message);
}
