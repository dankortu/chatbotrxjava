package com.example.RxChatbot;

import io.reactivex.Observable;

public interface IMainViewModel {
    Observable<Msg> getMsgSubject();

    void onMessageSend(String text);
}
