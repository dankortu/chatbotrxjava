package com.example.RxChatbot;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableSingleObserver;

public class DisposableSingleObserverUtils {
    static <T> DisposableSingleObserver<T> getSingleObserver(@NonNull OnSuccessCallback<T> successCallback, @NonNull OnErrorCallback errorCallback) {
        return new DisposableSingleObserver<T>() {
            @Override
            public void onSuccess(@NonNull T t) {
                successCallback.onSuccess(t);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                errorCallback.onError(e);
            }
        };
    }

    public interface OnSuccessCallback<T> {
        void onSuccess(T model);
    }

    public interface OnErrorCallback {
        void onError(Throwable throwable);
    }
}
