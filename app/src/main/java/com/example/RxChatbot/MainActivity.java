package com.example.RxChatbot;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.RxChatbot.databinding.ActivityMainBinding;

import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity {

    final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
    final Adapter adapter = new Adapter();
    final IMainViewModel viewModel = new MainViewModel();

    ActivityMainBinding binding;
    Disposable msgDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        final Button button = binding.buttonSend;

        button.setOnClickListener(this::handleMessageSend);

        binding.recycler.setLayoutManager(linearLayoutManager);
        binding.recycler.setAdapter(adapter);

        msgDisposable = viewModel.getMsgSubject().subscribe(this::handleMsgReceive);
    }

    private void handleMsgReceive(Msg msg) {
        adapter.addMessage(msg);
        linearLayoutManager.smoothScrollToPosition(binding.recycler, null, adapter.getItemCount());
    }

    private void handleMessageSend(View view) {
        final TextView tv = binding.inputText;

        if (!tv.getText().toString().isEmpty()) {
            String message = tv.getText().toString();
            tv.setText("");
            viewModel.onMessageSend(message);
        }
    }

    @Override
    protected void onDestroy() {
        if (msgDisposable != null && !msgDisposable.isDisposed()) {
            msgDisposable.dispose();
        }

        super.onDestroy();
    }
}