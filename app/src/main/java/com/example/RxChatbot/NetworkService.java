package com.example.RxChatbot;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public final class NetworkService {
    private static NetworkService instance;
    private static final String BASE_URL = "http://192.168.3.61:3000";
    private final Retrofit retrofit;

    public static NetworkService getInstance() {

        synchronized (NetworkService.class){
            if (instance == null) {
                instance = new NetworkService();
            }
            return instance;
        }

    }

    public NetworkService() {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY );
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(loggingInterceptor);
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient.build())
                .build();
    }

    public NodeServerApi getJSONApi() {
        return retrofit.create(NodeServerApi.class);
    }
}
